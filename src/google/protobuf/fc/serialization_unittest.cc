#include <gtest/gtest.h>

#include "helpers.h"

#include <fc/reflect/reflect.hpp>

#include "messages/unittest_basic.pb.h"

typedef struct
{
  uint32_t foo, bar;
} _b_fc_t;

FC_REFLECT( _b_fc_t, (foo) ( bar ) );

namespace google { namespace protobuf { namespace {

    TEST( SerializationTest, Simple )
    {
      basic _b;

      _b.set_foo( 1 );
      _b.set_bar( 2 );

      _b_fc_t _b_fc;

      _b_fc.foo = 1;
      _b_fc.bar = 2;

      EXPECT_EQ( helpers::protobuf_to_hex( _b ), helpers::fc_to_hex( _b_fc ) );
    }

}}} // namespace google::protobuf
