#pragma once

#include <string>
#include <vector>

#include <fc/io/raw.hpp>

namespace google { namespace protobuf { namespace helpers {

  std::string get_hex( const uint8_t* data, size_t len )
  {
    std::string result;
    result.resize( len * 2 );
    static const char* to_hex = "0123456789abcdef";
    uint8_t* c                = (uint8_t*) data;
    for( uint32_t i = 0; i < len; ++i )
    {
      result[i * 2]     = to_hex[( c[i] >> 4 )];
      result[i * 2 + 1] = to_hex[( c[i] & 0x0f )];
    }
    return result;
  }

  template< typename T >
  std::string pack_protobuf( const T& protobuf_obj )
  {
    std::string protobuf_data;
    protobuf_obj.SerializeToString( &protobuf_data );

    return protobuf_data;
  }

  template< typename T >
  std::vector< char > pack_fc( const T& fc_obj )
  {
    return fc::raw::pack_to_vector( fc_obj );
  }

  template< typename T >
  std::string protobuf_to_hex( const T& protobuf_obj )
  {
    std::string protobuf_data = pack_protobuf( protobuf_obj );

    return get_hex( reinterpret_cast< const uint8_t* >( protobuf_data.data() ), protobuf_data.size() );
  }

  template< typename T >
  std::string fc_to_hex( const T& fc_obj )
  {
    std::vector< char > fc_data = pack_fc( fc_obj );

    return get_hex( reinterpret_cast< const uint8_t* >( fc_data.data() ), fc_data.size() );
  }

}}} // namespace google::protobuf::helpers
